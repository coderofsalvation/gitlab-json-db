gitlab repository as a read/write json-database for the browser (using pipeline triggers)
<img src="https://gitlab.com/coderofsalvation/gitlab-json-db/raw/master/doc/diagram.png"/>


# Usage

> 1. Initialize

	$ npm install gitlab-json-db -g
    $ mkdir mysite
    $ cd mysite
    $ git init
    $ gdb init
	created .gitlab-ci.yml + bin-files + gldb.js

	Now us it like this:

			<script src="https://YOURORG.gitlab.io/YOURPROJECT/gldb.js"></script>
			<script>
					var db = gldb({
							trigger: "YOUR_GITLAB_PIPELINE_TRIGGER_URL",
							token: "YOUR_TOKEN",
							url: "https://YOURORG.gitlab.io/YOURPROJECT"
					})
					db.fruits = db.collection("fruits") 
					db.fruits.post({"apple":2}, alert, console.error)
			</script>

> 2. Viola, we have our necessary files:

	$ ls
    bin/config.sh
    bin/put
    bin/post
    bin/delete
    gldb.js
    .gitlab-ci.yml

Now edit `bin/config.sh` to define collection-to-repositories mappings  (for example `fruits` points to `git@gitlab.com:MYORG/fruits.git`)

> 3. Lets upload them:

	$ git add .
    $ git commit -m "1st commit"
    $ git push origin master


#### Trigger & Deploy key

> 4. Almost there. Now create a trigger in gitlab, to use the snippet mentioned in the pipeline output above

<img src="https://gitlab.com/coderofsalvation/gitlab-json-db/raw/master/doc/trigger.png"/>

> 5. Now on the database-repository(s) configure a new deploy key, and copy/paste the **private** value of the keyfile (not .pub) into the (gldb inited) repository:

<img src="https://gitlab.com/coderofsalvation/gitlab-json-db/raw/master/doc/deploykey.png"/>

> 6. Also create an environment 'SSH_PRIVATE_KEY'-variable, and copy/paste the **private** value of the keyfile in there 

Now the (gldb inited) has read/write access to your database repositorie(s).
Check the pipeline output to verify this.

> Done! Please check the previous steps in case things don't work. If things went fine: check the pipelines-log. A pipeline started to create a gitlab page (so gldb.js is accessible for the browser)

# Reference 

> NOTE: all calls return a status (a pipeline is triggered),  therefore it's not to be used for realtime output,  but rather aftermath.

| function call | comment |
|-|-|
| `db.mycollection.post({"foo":"bar"}, cb, cb_err)` | will create item in /mycollection.json |
| `db.mycollection.put({"id":"kjkj334$", "foo":"bar"}, cb, cb_err)` | will update item in /mycollection.json |
| `db.mycollection.delete({"id":"kjkj334$""}, cb, cb_err)` | will delete item in /mycollection.json |

> NOTE: reading data is optional: you can expose your jsonfiles to the web by deploying database-repositories (A & B) as gitlab pages.

# Why

* microstack experiment
* sometimes slow inserts/updates/deletions are good enough.

# Pros/cons

* pro: free, reliable hosting
* con: writes are slow (so use your browsermemory as database e.g.)
