window.gldb = function(opts){
    this.endpoint = function(collection){
        
        var me = this
        
        this.request = function(command,payload,cb,cb_err){
            var url = opts.trigger
            url += '?token='+opts.token
            url += '&ref=master'
            url += '&variables[collection]='+collection
            url += '&variables[command]='+command
            url += payload && payload.id ? '&variables[id]='+payload.id : ''
            url += '&variables[payload]='+encodeURIComponent(JSON.stringify(payload))
            fetch( url, { method:'POST' })
            .then( cb )
            .catch( cb_err )
        }

                
        this.put    = this.request.bind(this, 'put')
        this.post   = this.request.bind(this, 'post')
        this.delete = this.request.bind(this, 'delete')
                                       
        return this        
    }                      

    this.collection = function(collection){
      return new this.endpoint(collection)
    }
    return this
}

