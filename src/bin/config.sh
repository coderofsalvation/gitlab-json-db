declare -A collections
collections['fruits']='git@gitlab.com:YOURORG/fruits.git'

set -u
set -e
set -x

error(){
	echo "error: $1"
	exit 1
}

uid(){
  	set +x; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1; set -x
}

get_repo(){
	set +x;	printf "${collections[$collection]}"; set -x
}

get_repodir(){
	slug="$(basename "$(get_repo)")"
	dir="repo/$slug"
	[[ ! -d "$dir" ]] && mkdir "$dir"
	printf "$dir"
}

clone_repo(){
	repo=$(get_repo)
	repodir=$(get_repodir)
	if [[ ! -d "$repodir" ]]; then
		git clone "$repo" "$repodir"
		cd "$repodir"
	else
		cd "$repodir"
		git pull origin master -f 
	fi
	cd -
}

#trap 'rm -rf repo' 0 1 2 SIGINT SIGTERM
