module.exports = function(payload, data, process, resolve, reject){
  // pre-process payload here 
  process(data)
  // post-process data here  
  resolve(data)
}
